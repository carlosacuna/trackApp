import React, { Component } from 'react';
import LoadingLayout from '../../sections/components/loading';
import { connect } from 'react-redux';

class Ruta extends Component{
    componentDidMount() {
        if(!this.props.user) {
            this.props.navigation.navigate('Login');
        }

        if(this.props.ruta){
            console.log("paso en primera");
            this.props.navigation.navigate('RutaVista'); 
        }
      
        fetch('http://192.168.1.3:3000/getruta',
        {
          body: JSON.stringify({
            usuario: this.props.user.token
          }),
          headers:{
           'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: 'POST',
        }).then(response => response.json())
        .then((response) => { 
            console.log(response);

            if(response.msg){
                this.props.dispatch({
                    type: 'SET_RUTA',
                    payload: {
                        ruta: response.msg.idruta,
                      }
                });
               
                this.props.navigation.navigate('RutaVista'); 
            }else{
                console.log("no paso");
                this.props.navigation.navigate('RutaCrear'); 
            }
        });
        

        
      }

    render(){
        return <LoadingLayout />
    }
}


function mapStateToProps(state){
    return {
        user: state.user,
        ruta: state.ruta
    }
}

export default connect(mapStateToProps)(Ruta);